USE asterix;

-- Requêtes d’interrogation de la base de données : 
  
-- 1. Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes) 
SELECT * FROM potion;


-- 2. Liste des noms des trophées rapportant 3 points. (2 lignes) 

SELECT * FROM trophee
WHERE NumResserre ='3';
-- 3. Liste des villages (noms) contenant plus de 35 huttes.  (4 lignes) 
SELECT * FROM village
WHERE NbHuttes >'35';
-- 4. Liste des trophées (numéros) pris en mai / juin 52.  (4 lignes) 
SELECT * FROM trophee
WHERE DatePrise BETWEEN '52-05-05' AND '52-06-06';

-- 5. Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes) 
SELECT * FROM habitant
WHERE Nom LIKE 'a%r%' ;


-- 6. Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes) 
SELECT `NumHab` FROM absorber
WHERE NumPotion ='1' OR NumPotion ='3' AND NumPotion='4';
-- 7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10 
-- lignes) 
SELECT trophee.NumTrophee, trophee.DatePrise , categorie.NomCateg, habitant.Nom FROM trophee
JOIN categorie ON trophee.CodeCat= categorie.CodeCat
JOIN habitant ON trophee.NumPreneur= habitant.NumHab;
-- 8. Nom des habitants qui habitent à Aquilona. (7 lignes) 

SELECT habitant.nom FROM habitant
JOIN village on habitant.numVillage= village.numVillage
WHERE village.nomVillage ="Aquilona";
-- 9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes) 
SELECT habitant.nom FROM habitant
JOIN trophee on trophee.`NumPreneur`= habitant.NumHab
JOIN categorie on categorie.`CodeCat`= trophee.`CodeCat`
WHERE categorie.`NomCateg`= "Bouclier de Légat";
-- 10. Liste des potions (libellés)  fabriquées par Panoramix : libellé, formule et constituant principal. (3 lignes)
SELECT potion.LibPotion, potion.Formule, potion.ConstituantPrincipal FROM potion
JOIN fabriquer on fabriquer.`NumPotion`= potion.`NumPotion`
JOIN habitant on habitant.`NumHab`= fabriquer.`NumHab`
WHERE habitant.nom ="Panoramix";
 
-- 11. Liste des potions (libellés)  absorbées par Homéopatix.  (2 lignes) 
SELECT potion.LibPotion FROM potion
JOIN absorber on absorber.`NumPotion`= potion.`NumPotion`
JOIN habitant on habitant.`NumHab`= absorber.`NumHab`
WHERE habitant.`Nom` = "Homéopatix";
-- 12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3. (4 lignes)
SELECT habitant.Nom FROM habitant
JOIN absorber on absorber.`NumHab`= habitant.`NumHab`
JOIN potion on potion.`NumPotion`= absorber.`NumPotion`
JOIN fabriquer on fabriquer.`NumPotion`= potion.`NumPotion`
WHERE fabriquer.`NumHab`= 3;

-- 13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes) 
SELECT DISTINCT habitant.Nom FROM habitant
JOIN absorber on absorber.`NumHab`= habitant.`NumHab`
JOIN potion on potion.`NumPotion`= absorber.`NumPotion`
JOIN fabriquer on potion.`NumPotion`= fabriquer.`NumPotion`
WHERE fabriquer.`NumHab`= 2;
-- 14. Nom des habitants dont la qualité n'est pas renseignée. (3 lignes) 
SELECT Nom FROM habitant
WHERE NumQualite IS NULL;
-- 15. Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la 
-- potion) en février 52.  (3 lignes) 
SELECT h.Nom FROM habitant h
INNER JOIN absorber a ON h.NumHab = a.NumHab
INNER JOIN potion p ON a.NumPotion = p.NumPotion
WHERE p.LibPotion = 'Potion magique n°1' AND MONTH(a.DateA) = 2 AND YEAR(a.DateA) = 2052;
-- 16. Nom et âge des habitants par ordre alphabétique. (22 lignes) 
SELECT habitant.Nom FROM habitant
ORDER BY Nom ASC;

-- 17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes) *** 
SELECT NomResserre, NumVillage FROM resserre
ORDER BY NomResserre DESC;

-- 18. Nombre d'habitants du village numéro 5.  (4) 
SELECT COUNT(habitant.NumHab) FROM habitant
JOIN village on village.`NumVillage`= habitant.`NumVillage`
WHERE village.`NomVillage`='Condate';
-- 19. Nombre de points gagnés par Goudurix. (5) 
SELECT SUM(c.NbPoints) AS Points_Gagnes_Par_Goudurix FROM habitant h
INNER JOIN qualite q ON h.NumQualite = q.NumQualite
INNER JOIN trophee t ON h.NumHab = t.NumPreneur
INNER JOIN categorie c ON t.CodeCat = c.CodeCat
WHERE h.Nom = 'Goudurix';

-- 20. Date de première prise de trophée. (03/04/52) 
SELECT DatePrise FROM trophee
WHERE DatePrise = "2052-04-03";

-- 21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19) 
SELECT SUM(Quantite) AS Nombre_de_Louches FROM absorber a
INNER JOIN potion p ON a.NumPotion = p.NumPotion
WHERE p.LibPotion = 'Potion magique n°2';

-- 22. Superficie la plus grande. (895) *** 
SELECT Superficie FROM resserre
WHERE Superficie = 895;
-- 23. Nombre d'habitants par village (nom du village, nombre). (7 lignes) 
SELECT NomVillage, COUNT(NumHab) FROM habitant
JOIN village ON habitant.NumVillage = village.NumVillage
GROUP BY NomVillage;
-- 24. Nombre de trophées par habitant (6 lignes) 
SELECT habitant.Nom, COUNT(*) AS NumTrophee FROM trophee
INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab
GROUP BY habitant.Nom;
-- 25. Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes) 
SELECT p.NomProvince AS Nom_De_Province, AVG(h.Age) AS Moyenne_Age FROM habitant h
INNER JOIN village v ON h.NumVillage = v.NumVillage
INNER JOIN province p ON v.NumProvince = p.NumProvince
GROUP BY p.NomProvince;
-- 26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes) 
SELECT Nom, COUNT(DISTINCT absorber.NumPotion) FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
GROUP BY Nom;

-- 27. Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne) *** 
SELECT habitant.nom from habitant
JOIN absorber on habitant.NumHab = absorber.NumHab
JOIN potion on potion.NumPotion = absorber.NumPotion
where potion.LibPotion like '%zen%'
and absorber.quantite > 2
;

-- 28. Noms des villages dans lesquels on trouve une resserre (3 lignes) 
SELECT DISTINCT nomvillage from village
JOIN resserre on village.NumVillage =resserre.NumVillage
;

-- 29. Nom du village contenant le plus grand nombre de huttes. (Gergovie) 
SELECT nomvillage from village
WHERE village.NbHuttes ='55';
-- 30. Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes).

SELECT nom, COUNT(numtrophee) AS nombre_trophees FROM habitant
JOIN trophee on trophee.numpreneur= habitant.numhab
GROUP BY nom
HAVING COUNT(numtrophee) >= (SELECT(numtrophee) from trophee
JOIN habitant on trophee.numpreneur = habitant.numhab
WHERE habitant.nom = "Obelix");